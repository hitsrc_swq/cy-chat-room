# cy-chat-room

 **html纯前端框架  cy-ui  [http://ui.cymall.xin](http://ui.cymall.xin)** 


#### 项目介绍
基于netty+websocket的在线聊天室

#### 使用技术
netty5 + webSocket + springboot + freemarker + layui

#### 使用说明

1. 下载源码
2. 运行AdminApplication类
3. 输入地址 [http://localhost:8001](http://localhost:8001)

#### 项目截图

![首页](https://images.gitee.com/uploads/images/2018/0829/211328_817d7b49_1334796.png "屏幕截图.png")

![聊天界面](https://images.gitee.com/uploads/images/2018/0829/211400_15088bc2_1334796.png "屏幕截图.png")

![鲜花雨](https://images.gitee.com/uploads/images/2018/0829/211418_aff1d8c5_1334796.png "屏幕截图.png")


#### 捐赠作者

如有帮助到您，请作者喝杯咖啡吧！

![输入图片说明](https://gitee.com/uploads/images/2018/0106/184140_fd082023_1334796.png "屏幕截图.png")
